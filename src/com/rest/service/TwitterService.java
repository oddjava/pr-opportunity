package com.rest.service;

import java.util.List;

import org.hibernate.SessionFactory;

import com.rest.dao.TwitterDao;
import com.rest.entity.Twitter;

public class TwitterService 
{
	private TwitterDao twitterDao;

	public TwitterDao getTwitterDao() {
		return twitterDao;
	}

	public void setTwitterDao(TwitterDao twitterDao) {
		this.twitterDao = twitterDao;
	}
	
	public void addTwitterPerson(Twitter twitter , String imageName) throws Exception
	{
		getTwitterDao().insert(twitter,imageName);
	}
	/*public List<Twitter> fetchSomePersonFromTwitter()
	{
		return getTwitterDao().fetchSomePersonFromTwitter();
	}*/
}
