package com.rest.service;

import java.util.List;

import com.rest.dao.SearchkeywordDao;
import com.rest.entity.Searchkeyword;
import com.rest.main.MyException;

public class SearchkeywordService
{
	SearchkeywordDao searchKeyWordDao;

	public SearchkeywordDao getSearchKeyWordDao() {
		return searchKeyWordDao;
	}

	public void setSearchKeyWordDao(SearchkeywordDao searchKeyWordDao) {
		this.searchKeyWordDao = searchKeyWordDao;
	}
	public void addSearchKeywordForTwitter(Searchkeyword searchKeyWordObject) throws MyException
	{
		getSearchKeyWordDao().insert(searchKeyWordObject);
	}
	public List<Searchkeyword> listAllSearchKeyWordsForTwitter()
	{
		
		return searchKeyWordDao.getAllKeyWords();
		
	}
}
