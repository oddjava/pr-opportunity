package com.rest.mongo.client.singleton;

import com.mongodb.MongoClient;

public class GetMongoClient {

	
		private static final MongoClient instance = new MongoClient("178.32.49.5", 27017);
		
	    //private constructor to avoid client applications to instantiate
	    private GetMongoClient(){}

	    public static MongoClient getConnection(){
	        return instance;
	    }
	    
	    //Closing Connection here
	    protected void finalize() throws Throwable {
			// TODO Auto-generated method stub
			instance.close();
			super.finalize();
		}

}
