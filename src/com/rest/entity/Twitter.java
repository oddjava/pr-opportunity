package com.rest.entity;


public class Twitter
{
	private Long tableId;
	private String name;
	private String screen_name;
	private String location;
	private String description;
	private Long followers_count;
	private byte[] profile_image;
	private byte[] twitter_image;
	private String tweettime;
	private String profile_url;
	private String domainLink;
	private String searchkeyword;
	private String image_name;
	
	public String getImage_name() {
		return image_name;
	}
	public void setImage_name(String image_name) {
		this.image_name = image_name;
	}
	public String getSearchkeyword() {
		return searchkeyword;
	}
	public void setSearchkeyword(String searchkeyword) {
		this.searchkeyword = searchkeyword;
	}
	public String getDomainLink() {
		return domainLink;
	}
	public void setDomainLink(String domainLink) {
		this.domainLink = domainLink;
	}
	public byte[] getTwitter_image() {
		return twitter_image;
	}
	public void setTwitter_image(byte[] twitter_image) {
		this.twitter_image = twitter_image;
	}
	private String systemDate;
	
	
	public String getSystemDate() {
		return systemDate;
	}
	public void setSystemDate(String systemDate) {
		this.systemDate = systemDate;
	}
	
	
	public String getProfile_url() {
		return profile_url;
	}
	public void setProfile_url(String profile_url) {
		this.profile_url = profile_url;
	}
	public String getTweettime() {
		return tweettime;
	}
	public void setTweettime(String tweettime) {
		this.tweettime = tweettime;
	}
	

	public Long getTableId() {
		return tableId;
	}
	public void setTableId(Long tableId) {
		this.tableId = tableId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getScreen_name() {
		return screen_name;
	}
	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getFollowers_count() {
		return followers_count;
	}
	public void setFollowers_count(Long followers_count) {
		this.followers_count = followers_count;
	}
	public byte[] getProfile_image() {
		return profile_image;
	}
	public void setProfile_image(byte[] profile_image) {
		this.profile_image = profile_image;
	}
	
	public boolean equals(Object obj)
	{
		if(obj==null)
			return false;
		if(!this.getClass().equals(obj.getClass()))
			return false;
		Twitter twitter=(Twitter)obj;
		
		if((this.tableId==twitter.getTableId())&&(this.name.equals(twitter.getName())))
		{
			return true;
		}
		return false;
	}
	public int hashCode()
	{
		int temp=0;
		temp=(tableId+name).hashCode();
		return temp;
	}
}
