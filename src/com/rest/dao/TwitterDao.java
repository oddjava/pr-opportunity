package com.rest.dao;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.load.LoadAvg;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;
import com.rest.entity.Twitter;
import com.rest.main.GeoLocation;
import com.rest.main.S3upload;
import com.rest.main.SaveImage;
import com.rest.mongo.client.singleton.GetMongoClient;

public class TwitterDao {
	static MongoClient mongoClient = GetMongoClient.getConnection();
	static DB db = mongoClient.getDB("crawler");
	static DBCollection crawldataDB = db.getCollection("crawldata");
	static DBCollection samplecrawldataDB = db.getCollection("sampleCrawldata");
	static DBCollection titleDescriptionDB = db.getCollection("titleDescription");
	DBCollection dataCountDB = db.getCollection("dataCount");

	/*
	 * MongoClient mongoClient = new MongoClient("localhost", 27017); DB db
	 * =mongoClient.getDB("twitter"); DBCollection crawldataDB =
	 * db.getCollection("twitterforPR");
	 */

	private SessionFactory sessionFactory;

	/*
	 * public SessionFactory getSessionFactory() { return sessionFactory; }
	 * public void setSessionFactory(SessionFactory sessionFactory) {
	 * this.sessionFactory = sessionFactory; }
	 */
	public void insert(Twitter twitter, String imageName) throws Exception {

		Double load;
		load = new LoadAvg().getLoadAvg();
		if (load <= 1.50) {
			String twitLoc = twitter.getLocation();
			String geoLocation = GeoLocation.updateLocation(twitLoc);
			BasicDBObject insertObject = new BasicDBObject();
			DBObject dbObject;
			int countFlag = 0;
			int id = 0;
			String link = twitter.getProfile_url();
			DBCursor samplecrawldataDBcursor = samplecrawldataDB.find(new BasicDBObject("link", link));

			if (samplecrawldataDBcursor.count() > 0) {
				return;
			}

			/*
			 * geoip initialization code
			 */

			if (geoLocation.isEmpty()) {
				dbObject = (DBObject) JSON.parse(
						"{" + "'type' : 'MultiPoint'," + "'coordinates' : [ " + "[ 0.00, 0.00 ]" + "]" + "}" + "}");
			} else {
				try {
					BasicDBObject doc = new BasicDBObject();
					String l[] = geoLocation.split("/");

					String query = "'coordinates' : [ ";
					for (int i = 0; i < l.length; i++) {
						String lon[] = l[i].split(",");
						if (i == l.length - 1) {
							query += "[" + lon[1] + "," + lon[0] + "]";

						} else
							query += "[" + lon[1] + "," + lon[0] + "],";
					}
					query += "]";

					dbObject = (DBObject) JSON.parse("{" + "'type' : 'MultiPoint'," + query + "" + "}}");
				} catch (Exception e) {
					dbObject = (DBObject) JSON.parse("{'geoip' : {" + "'type' : 'MultiPoint'," + "'coordinates' : [ "
							+ "[ 0.00, 0.00 ]" + "]" + "}" + "}");

				}
			}

			/*
			 * Insering data in Mongo checking for Duplicates
			 */
			try {

				// insertObject.clear();
				// insertObject.put("title", twitter.getName());
				// insertObject.put("description", new
				// TwitterDao().removeUrl(twitter.getDescription()));
				insertObject.clear();
				insertObject = new BasicDBObject("titleDescription", (twitter.getName().toString()
						+ new TwitterDao().removeUrl(twitter.getDescription()).replaceAll("\\s*", "")));
				if (!titleDescriptionDB.find(insertObject).hasNext()) {
					titleDescriptionDB.insert(insertObject);
					if (!samplecrawldataDBcursor.hasNext()) {
						String title, description;
						title = twitter.getName();
						title = title.replaceAll("[^0-9a-zA-Z ]", " ").replaceAll(" +", " ").trim();
						title = StringUtils.stripAccents(title);

						description = twitter.getDescription();
						description = description.replaceAll("[^0-9a-zA-Z#@:/. ]", " ").replaceAll(" +", " ").trim();
						description = StringUtils.stripAccents(description);

						insertObject.clear();
						id = getNextSequence("userid");
						insertObject.put("_id", id);
						insertObject.put("domainLink", "twitter.com" + "/" + twitter.getScreen_name());
						insertObject.put("link", twitter.getProfile_url());
						insertObject.put("title", title);
						insertObject.put("authorityTitle", title);
						insertObject.put("description", description);
						insertObject.put("keyword", twitter.getSearchkeyword());
						insertObject.put("leadDate", twitter.getTweettime());
						insertObject.put("subCategory", "PrOpportunities");
						insertObject.put("expiredDate", "");
						insertObject.put("expiredStatus", "live");
						insertObject.put("location", twitter.getLocation());
						insertObject.put("geoLocation", geoLocation);
						insertObject.put("personName", twitter.getScreen_name());
						insertObject.put("systemDate", twitter.getSystemDate());
						insertObject.put("image", twitter.getImage_name());
						insertObject.put("moduleName", "News");
						insertObject.put("status", "true");
						insertObject.put("authorityStatus", "true");
						insertObject.put("boilerpipeStatus", "true");
						insertObject.put("nerStatus", "true");
						insertObject.put("imageStatus", "true");
						insertObject.put("translationStatus", "true");
						insertObject.put("geoip", dbObject);

						samplecrawldataDB.insert(insertObject);
						crawldataDB.insert(insertObject);
						/*
						 * Uploading Image to S3 bucket
						 */
						new S3upload(imageName);
						insertObject.clear();

						System.out.println("Link is " + twitter.getProfile_url());
						System.out.println("PR Opportunity is " + description);
					} else {
						System.out.println("Link is duplicate");
					}
				} else {
					System.out.println("Tweet is double");
				}

				samplecrawldataDBcursor.close();

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Exception while inserting...");
			}
			/*
			 * catch(DuplicateKeyException e) {
			 * System.out.println(descreaseSequence("userid")); }
			 */

		} else {
			System.out.println("In Twitter Dao.......");
			System.out.println("load exceeded with Load : " + load);
			System.exit(0);
		}
	}

	/*
	 * public List<Twitter> fetchSomePersonFromTwitter() { Session
	 * session=getSessionFactory().getCurrentSession();
	 * session.beginTransaction(); Criteria
	 * criteria=session.createCriteria(Twitter.class); List<Twitter>
	 * list=criteria.list(); session.getTransaction().commit(); return list; }
	 */

	public String removeUrl(String commentstr) {
		String cleartext = commentstr.replaceAll("http.*?\\s", " ");
		return cleartext.replaceAll(" +", " ").trim();
	}

	public static int getNextSequence(String name) throws Exception {
		// MongoClient mongoClient = new MongoClient( "178.32.61.54" , 27017 );
		MongoClient mongoClient = GetMongoClient.getConnection();
		// Now connect to your databases
		DB db = mongoClient.getDB("crawler");
		DBCollection collection = db.getCollection("counters");
		BasicDBObject find = new BasicDBObject();
		find.put("_id", name);
		BasicDBObject update = new BasicDBObject();
		update.put("$inc", new BasicDBObject("seq", 1));
		DBObject obj = collection.findAndModify(find, update);
		return (int) obj.get("seq");
	}

	/*
	 * public static int descreaseSequence(String name) throws Exception {
	 * 
	 * MongoClient mongoClient = GetMongoClient.getConnection(); DB db =
	 * mongoClient.getDB("crawler"); // DB db = mongoClient.getDB("Journalist");
	 * DBCollection collection = db.getCollection("counters"); BasicDBObject
	 * find = new BasicDBObject(); find.put("_id", name); BasicDBObject update =
	 * new BasicDBObject(); update.put("$inc", new BasicDBObject("seq", -1));
	 * DBObject obj = collection.findAndModify(find, update); return
	 * (Integer)obj.get("seq"); }
	 */

}
