package com.rest.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.rest.entity.Searchkeyword;
import com.rest.entity.Twitter;
import com.rest.main.MyException;

public class SearchkeywordDao 
{
	SessionFactory sessionFactory;
	
	
	public SessionFactory getSessionFactory() 
	{
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) 
	{
		this.sessionFactory = sessionFactory;
	}
	public void insert(Searchkeyword searchKeyWordObject) throws MyException  
	{
		try
		{
			Session session=getSessionFactory().getCurrentSession();
			session.beginTransaction();
			session.save(searchKeyWordObject);
			session.getTransaction().commit();
		}
		catch(org.hibernate.exception.ConstraintViolationException e)
		{
			getSessionFactory().getCurrentSession().getTransaction().rollback();
			
				throw new MyException(e.getCause());
		}
	}
	public List<Searchkeyword> getAllKeyWords()
	{
		Session session=getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Criteria criteria=session.createCriteria(Searchkeyword.class);
		List<Searchkeyword> list=criteria.list();
		session.getTransaction().commit();
		return list;
	}

}
