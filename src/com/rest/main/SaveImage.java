package com.rest.main;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;

import com.sun.java_cup.internal.runtime.Symbol;

public class SaveImage 
{
	private static final String folderPath = "Images"+File.separator;
	//It returns Fileinputstream for image file 
	
				public String save_twitter_image(String tweet_imageurl) throws IOException			
				{
				tweet_imageurl = tweet_imageurl.replace("_normal","_400x400");
				// String folder = null;
					int counter =1;
				InputStream inStream = null;
				OutputStream outStream = null;
				FileInputStream fis = null;
			//	System.out.println("Image url "+tweet_imageurl);
				// Exctract the name of the image from the src attribute
				int indexname = tweet_imageurl.lastIndexOf("/");
				//System.out.println("Image Url : "+tweet_imageurl);
				if (indexname == tweet_imageurl.length()-1) 
				{
					tweet_imageurl = tweet_imageurl.substring(1, indexname);
				}

				indexname = tweet_imageurl.lastIndexOf("/");
				String name_tweetimg = tweet_imageurl.substring(indexname, tweet_imageurl.length());
				name_tweetimg = name_tweetimg.replaceFirst("/", "");
				//System.out.println("Name of a file:" + name_tweetimg);
				// Open a URL Stream
				try 
				{
					URL url1 = new URL(tweet_imageurl);// a pointer to a "resource" on the World Wide Web
					inStream = url1.openStream();// Opens a connection to this URL and
													// returns an InputStream for
													// reading from that connection.
					outStream = new BufferedOutputStream(new FileOutputStream
							(folderPath + name_tweetimg));// Image will be saved at "folderPath"
				
					counter++;
					for (int b; (b = inStream.read()) != -1;) 
					{
						outStream.write(b);
					}

					File file;
					// String name1=name.substring(1);
					
					file = new File(folderPath + name_tweetimg);
					fis = new FileInputStream(file);
					//System.out.println("...."+fis);
					//System.out.println("File name is" + folderPath.concat(file.getName()));
					
				}
				catch (Exception e)
				{
						e.printStackTrace();
						System.out.println(e.getMessage());
				} 
				finally
				{
						outStream.close();
						inStream.close();

				}
				return name_tweetimg;//.substring(1);
				}
}

