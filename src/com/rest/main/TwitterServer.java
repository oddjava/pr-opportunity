package com.rest.main;

import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.load.LoadAvg;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.rest.entity.Twitter;
import com.rest.service.TwitterService;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

public class TwitterServer implements ApplicationContextAware {
	
	private String AccessToken;
	private String AccessSecret;
	private String ConsumerKey;
	private String ConsumerSecret;
	private int pageCounter = 0;
	private String searchKeyWord;
	private OAuthConsumer consumer;
	private ApplicationContext applicationContext;
	TwitterService twitterService;
	private HashSet<Twitter> twitterData;
	String kyword;
	int no_Of_request = 0;
	SaveImage saveimage=new SaveImage();

	public HashSet getTwitterData() 
	{
		return twitterData;

	}

	public void setTwitterData(HashSet<Twitter> twitterData) {
		this.twitterData = twitterData;
	}

	public void doOAuth() {
		/*
		 * AccessToken and AccessSecret are obtained from the twitter account
		 * after registering the app ConsumerKey ConsumerSecret belongs to per
		 * developer
		 */
		consumer = new CommonsHttpOAuthConsumer(ConsumerKey, ConsumerSecret);

		consumer.setTokenWithSecret(AccessToken, AccessSecret);
		twitterService = applicationContext.getBean("twitterService", TwitterService.class);
	}

	
	public void getDataFromTwitterIntoDb(boolean flag, String SearchKeyWord)	
	{
		
		try {
				saveTwitterPerson(flag, SearchKeyWord);
			} 
		
		catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException e) 
		{
			try
			{	
				saveTwitterPerson(flag, SearchKeyWord);
			} 
			catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException e1) {
				e1.printStackTrace();
				System.err.println("OAuth problem");
			}
		} 

		//return twitterData;
	}
	
	/*
	 * This function uses twitter api and sends the keyword to api and gets the persons data in JSON array
	 * 
	 */
	

	public void saveTwitterPerson(boolean flag, String SearchKeyWord) throws OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException
	{

		String profile_url = "https://twitter.com/";
		twitterData.clear();
		Date date = new Date();
		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String systemdate = sm.format(date);
		String searchKeyword=SearchKeyWord;
		/*
		 * rest api twitter query q param tells about the search and page
		 * param tells about the page number
		 */
		try {

			SearchKeyWord = "%23" + SearchKeyWord;
			String query = "https://api.twitter.com/1.1/search/tweets.json?q=" + SearchKeyWord + "&result_type=recent&count=20";

			HttpGet request = new HttpGet(query);

			consumer.sign(request);

			HttpClient client = new DefaultHttpClient();
			String jsonOp;

			HttpResponse response = client.execute(request);

			jsonOp = IOUtils.toString(response.getEntity().getContent());
			//System.out.println("First Executing : " + jsonOp);

			/*
			 * output is in json arry which is collection of json objects
			 */
			
			JSONObject mainobj = new JSONObject(jsonOp);
			JSONArray jsonarray = mainobj.getJSONArray("statuses");
			Twitter twitter;
			String sampleadDate,leadDate, imageName;

			Double load;
			
			for (int i = 0; i < jsonarray.length(); i++) 
			{
				load = new LoadAvg().getLoadAvg();
				if (load <= 1.50) {
				try {
					JSONObject obj = jsonarray.getJSONObject(i);
					if(!obj.getString("text").substring(0,6).contains("RT @") && obj.getString("text").contains(searchKeyword)) 
					{
						twitter = new Twitter();

						sampleadDate = obj.getString("created_at");
						leadDate = sampleadDate.substring(0, 11) + " "+ sampleadDate.substring(sampleadDate.length() - 4, sampleadDate.length());

						twitter.setName(obj.getJSONObject("user").getString("name"));
						twitter.setScreen_name(obj.getJSONObject("user").getString("screen_name"));
						twitter.setTweettime(leadDate);
						twitter.setLocation(obj.getJSONObject("user").getString("location"));
						twitter.setDescription(obj.getString("text"));
						twitter.setProfile_url(profile_url + obj.getJSONObject("user").getString("screen_name")+"/status/"+obj.getString("id_str"));
						twitter.setDomainLink(profile_url + obj.getJSONObject("user").getString("screen_name"));
						twitter.setFollowers_count(obj.getJSONObject("user").getLong("followers_count"));
						twitter.setSystemDate(systemdate);
						twitter.setSearchkeyword(kyword);
						/*
						 *Saving Image to local ./Images directory.
						 *This is needed because we use this local image to upload on S3 Bucket
						 * 
						 */
						imageName=saveimage.save_twitter_image(obj.getJSONObject("user").getString("profile_image_url"));
						twitter.setImage_name(imageName);
						
						/*
						 * if key words are not from cmd
						 */
						if (!flag) 
						{
							twitterService.addTwitterPerson(twitter,imageName);
						} 
						//else 
						{
							//System.out.println("twitter add...!");
							//twitterData.add(twitter);
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
			}else {
				System.out.println("In Twitter Server.......");
				System.out.println("load exceeded with Load : " + load);
				System.exit(0);
			}
			}

		} catch (org.json.JSONException e) {
			System.out.println(e.getLocalizedMessage());
			pageCounter = 1;

		} catch (org.hibernate.exception.ConstraintViolationException e) {
			System.out.println(e.getCause());
			System.out.println("duplicate entires not gone");

		} catch (ClientProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


	}

	
	
	public String getAccessToken() {
		return AccessToken;
	}

	public void setAccessToken(String accessToken) {
		AccessToken = accessToken;
	}

	public String getAccessSecret() {
		return AccessSecret;
	}

	public void setAccessSecret(String accessSecret) {
		AccessSecret = accessSecret;
	}

	public String getConsumerKey() {
		return ConsumerKey;
	}

	public void setConsumerKey(String consumerKey) {
		ConsumerKey = consumerKey;
	}

	public String getConsumerSecret() {
		return ConsumerSecret;
	}

	public void setConsumerSecret(String consumerSecret) {
		ConsumerSecret = consumerSecret;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		// TODO Auto-generated method stub
		this.applicationContext = applicationContext;
	}

}
