package com.rest.main;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.load.LoadAvg;
import com.rest.entity.Searchkeyword;
import com.rest.entity.Twitter;
import com.rest.service.SearchkeywordService;

//Twitter for PR opportunities

public class TwitterMain {
	public static void main(String args[]) {

		// Delete Image as it won't require
		deleteImage();
		/*
		 * Receive application context from spring.xml
		 */
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");
		SearchkeywordService searchKeyWordService = applicationContext.getBean("searchKeyWordService",
				SearchkeywordService.class);
		TwitterServer twitterServer = applicationContext.getBean("twitterServer", TwitterServer.class);
		InsertExcelToDB insertexceltodb = new InsertExcelToDB();
		// insertexceltodb.insertexceltodb();
		/*
		 * oauth is done for the app
		 */
		twitterServer.doOAuth();

		{
			// ArrayList<Searchkeyword> keyWordsal=(ArrayList<Searchkeyword>)
			// searchKeyWordService.listAllSearchKeyWordsForTwitter();

			HashSet<String> allKeywords = new HashSet<>();
			allKeywords = insertexceltodb.insertexceltoList();
			int no_Of_request = 0;
			long totalTime = 0, endTime = 0;
			long startTime = System.currentTimeMillis();

			// fetch the data from the twitter and store it in database
			Double load;
			for (String searchKeyword : allKeywords) {
				load = new LoadAvg().getLoadAvg();
				if (load <= 1.50) {
					totalTime = endTime - startTime;
					restart1:

					if (no_Of_request >= 179 && totalTime <= 900000) {
						try {
							Thread.sleep(900000 - totalTime);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						startTime = System.currentTimeMillis();
						no_Of_request = 0;
						// continue;
						break restart1;

					}
					twitterServer.getDataFromTwitterIntoDb(false, searchKeyword);
					no_Of_request++;
					endTime = System.currentTimeMillis();
				} else {
					endTime = System.currentTimeMillis();
					totalTime = endTime - startTime;
					System.out.println("In Main.......");
					System.out.println("load exceeded with Load : " + load + "Total Time Code executed : " + totalTime);
					System.exit(0);
				}
			}
			System.out.println("Done Total Number of:" + no_Of_request + "requests...");
		}

		System.out.println("Done with PR Execution....");

	}

	public static void deleteImage() {
		File file = new File("Images/");
		System.out.println(file.isDirectory());
		try {
			FileUtils.cleanDirectory(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
