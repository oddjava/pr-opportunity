package com.rest.main;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Pattern;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.rest.mongo.client.singleton.GetMongoClient;


public class GeoLocation
{
	//static MongoClient mongoClient = new MongoClient("178.32.61.54", 27017);
	
	static MongoClient mongoClient= GetMongoClient.getConnection();
	static DB db1=mongoClient.getDB("crawler");
	static DBCollection crawldataDB1= db1.getCollection("geodata");
	
	public static String updateLocation(String alllocation) 
	{
		String allLatlong = "",latlong="";
		DBObject present;
		DBCursor cursor;
		try
		{
					
			Set<String> locationset = new LinkedHashSet<>();
			String[] locations = alllocation.split(",");
			for (int i = 0; i < locations.length; i++) {
				// System.out.println(locations[i]);
				locationset.add(locations[i].trim());
			}
			for (String location : locationset) {
				BasicDBObject query = new BasicDBObject();
				
				//ref.put("myfield", Pattern.compile(".*myValue.*" , Pattern.CASE_INSENSITIVE));
				location= location.replaceAll("[-+.^:,]","");
				query.put("cityname", Pattern.compile("^" + location + "$", Pattern.CASE_INSENSITIVE));	//parameterized query. here we provide the condition
				cursor = crawldataDB1.find(query);	//If we don not have query then keep brackets blank. It will fetch all records
				if(cursor.count()==0)
				{
					latlong="0.0,0.0";
					allLatlong = allLatlong + latlong + "/";
				}
				
				while (cursor.hasNext())
				{
				   // System.out.println(cursor.next());
					present=cursor.next();
					latlong=present.get("latlong").toString();
					allLatlong = allLatlong + latlong + "/";
				}			
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return allLatlong;
	}
	
	public static void main(String[] args) 
	{
		
		String geoLoc=GeoLocation.updateLocation("pune,kolhapur,mumbai,solapur, Maharashtra, IN,Guiyang");
		//String geoLoc=GeoLocation.updateLocation("Austin,TX,Tx");
		System.out.println("geoLoc : "+geoLoc);
	}
}
