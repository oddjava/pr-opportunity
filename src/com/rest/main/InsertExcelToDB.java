package com.rest.main;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashSet;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

import com.rest.entity.Searchkeyword;


public class InsertExcelToDB 
{
	
	public Connection connectToDataBase() throws Exception 
	 {
		 Connection con = null;
		 Class.forName("com.mysql.jdbc.Driver");
		 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/leadlake_twitterhashtag", "root", "");
		 System.out.println("Connected database successfully ");
		 return con;
	 }
	
		 
		 public void insertexceltodb()
		 {
			 String searchKeyWordName=null;
			 try
			 {
	         
			 Connection con1= connectToDataBase();
			 System.out.println("Connected");
	         PreparedStatement pstm = null ;
	         FileInputStream input = new FileInputStream(new File("./keywords.xls"));
	         POIFSFileSystem fs = new POIFSFileSystem( input );
	         HSSFWorkbook wb = new HSSFWorkbook(fs);
	         HSSFSheet sheet = wb.getSheetAt(0);
	         Row row;
	         
	         for(int i=0; i<=sheet.getLastRowNum(); i++)
	         {
	             row = sheet.getRow(i);             
	             searchKeyWordName = row.getCell(0).getStringCellValue();
	             //String link = row.getCell(1).getStringCellValue();
	         
	             pstm = con1.prepareStatement("INSERT INTO searchkeyword (searchKeyWordName) VALUES(?)");   
	             pstm.setString(1,searchKeyWordName);
	            
	             pstm.executeUpdate();
	             
	         }       
	        
	         pstm.close();
	         con1.close();
	        // input.close();
	         System.out.println("Success import excel to mysql table");
	     }
		 
		 catch(Exception e)
		 {
			 System.out.println("can't insert value: "+searchKeyWordName);
	         
	     }
	 

	 } 
		 
		 
		 
		 
		 
		 public HashSet<String> insertexceltoList()
		 {
			 String searchKeyWordName=null;
			 HashSet<String> keyWordsal=new HashSet<>();
			
			 try
			 {
	         
			
	         FileInputStream input = new FileInputStream(new File("./PR_Keywords.xls"));
	         POIFSFileSystem fs = new POIFSFileSystem( input );
	         HSSFWorkbook wb = new HSSFWorkbook(fs);
	         HSSFSheet sheet = wb.getSheetAt(0);
	         Row row;
	         
	         for(int i=0; i<=sheet.getLastRowNum(); i++)
	         {
	             row = sheet.getRow(i);             
	             searchKeyWordName = row.getCell(0).getStringCellValue();
	             keyWordsal.add(searchKeyWordName);      
	           
	             
	         }   
	         
	     
	     }
		 
		 catch(Exception e)
		 {
			 e.printStackTrace();
	         
	     }
			 return keyWordsal;
	 

	 } 
		 
		 public static void main(String[] args) 
		 {
			// HashSet<String> allKeywords=new HashSet<>();
			// InsertExcelToDB insertexceltoDB=new InsertExcelToDB();
			// insertexceltoDB.insertexceltoList();
		 }
}

